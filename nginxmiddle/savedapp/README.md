# Checkpoint / saves of plain text app related files

In particular the .wsgi and .ini related to the app


## For reference only

The files in this directory are one time saves and not generally
part of a recommended build method

Automated build methods such as below:
/usr/bin/salt-call --local state.sls nginx112uwsgi
... would generally include the appropriate entry in /etc/uwsgi.d/
... making the copy stored here redundant (and possibly out of date)
