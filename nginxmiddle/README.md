# Nginx 1.12 on CentOS 6

Amzn 1 version: nginx-1.12.1-1.33.amzn1


## Need salt-call binary (on CentOS 6 this lives with minion)

yum --enablerepo=epel install salt-minion

# Next turn off the minion because we just wanted salt-call available
chkconfig --levels 0123456 salt-minion off

/usr/bin/salt-call --local state.sls nginx112uwsgi