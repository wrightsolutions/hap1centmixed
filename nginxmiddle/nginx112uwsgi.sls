# Nginx and uwsgi (emporer mode)
{% set approot = '/var/www/html' %}
{% set giuser = 'uwsgi' %}
{% set gigroup = 'uwsgi' %}
{% set nuser = 'nginx' %}
{% set ngroup = 'nginx' %}

nginx_uwsgi:
  # Install Web Server and wsgi daemon with py2 plugin
  pkg.installed:
  - pkgs:
    - nginx
    - uwsgi
    - uwsgi-plugin-python2


approot_dir:
  # Create directory for the approot if does not exist and give it nginx like permissions
  file.directory:
  - name: {{ approot }}
  - user: root
  - group: {{ ngroup }}
  - mode: 2755
  - makedirs: true
  - clean: False
  - exclude_pat: '*.*htm*'
  - require:
    - pkg: nginx_uwsgi
  # Require above could be omitted but stops any directory change unless nginx installed


uwsgi_run_dir:
  # Set perms/group wider than default: drwxrwxr-x 2 uwsgi uwsgi 4096 Jul  9 04:01 /var/run/uwsgi
  # Alternatively or additionally you can force this in uwsgi conf itself
  file.directory:
  - name: /var/run/uwsgi
  - user: {{ giuser }}
  - group: {{ ngroup }}
  - mode: 2755
  - clean: False
  - exclude_pat: '*.sock'
  - require:
    - pkg: nginx_uwsgi


uwsgi_log_dir:
  # Dedicated logging subdir below /var/log and used mainly by custom webapps
  file.directory:
  - name: /var/log/uwsgi/
  - user: root
  - group: {{ gigroup }}
  - mode: 2770
  - clean: False
  - exclude_pat: '*.log'
  - require:
    - pkg: nginx_uwsgi


uwsgi_main_conf:
  # Uwsgi main configuration file. Changed two lines from default so /var/run/ prefix
  # Typically this file is root:root and 644 so leaving those default settings unchanged
  file.managed:
  - name: /etc/uwsgi.ini
  - user: root         # {{ giuser }}
  - group: root        # {{ gigroup }}
  - mode: 644
  - contents: |
      ### Uwsgi configuration to fit into standard install involving /etc/uwsgi.d/
      [uwsgi]
      uid = uwsgi
      gid = uwsgi
      pidfile = /var/run/uwsgi/uwsgi.pid
      emperor = /etc/uwsgi.d
      stats = /var/run/uwsgi/stats.sock
      chmod-socket = 660
      emperor-tyrant = true
      cap = setgid,setuid
  - backup: minion


uwsgi_d_app_conf:
  # Uwsgi configuration file for our app
  # Ownership other than uwsgi:uwsgi is untested and may lead to 
  # log error '[emperor-tyrant] invalid permissions for vassal apphello_uwsgi'
  file.managed:
  - name: /etc/uwsgi.d/apphello_uwsgi.ini
  - user: {{ giuser }}
  - group: {{ gigroup }}
  - mode: 644
  - contents: |
      ### Uwsgi configuration to fit into standard install involving /etc/uwsgi.d/
      # On centos in /etc/uwsgi.d/apphello_uwsgi.ini
      [uwsgi]
      # Remember that pip installed uwsgi only supports compile time plugins so directives
      # ... plugin and plugin-dir would not fit with that build style.
      # Language/modifier/magicnumber support for Python(2) by adding a plugins entry
      #plugins-dir = /usr/lib64/uwsgi    
      plugin = python           
      # yum install uwsgi-plugin-python2     enables you to have above 'plugin = python'
      # If you insist on above being =python2 then cd /usr/lib64/;ln -s uwsgi/python_plugin.so uwsgi/python2_plugin.so
      python-path = /var/www/html
      # Set chdir to the fullpathed base directory where you web app lives below
      chdir = /var/www/html
      # Web app wsgi file
      wsgi-file = apphello.wsgi
      #module = apphello.wsgi
      # If virtual env installed then set home to full path of the virtualenv
      #home = /opt/local/py3venv
      # Logs
      logdate = True
      logto = /var/log/uwsgi/apphello_access.log
      ## Below are the process-related settings
      # master uwsgi?
      master = true
      # maximum number of worker processes to start
      processes = 5
      #limit-as = 128		# 128MB maximum
      # uwsgi socket (fullpathed or ip:port)
      socket = 127.0.0.1:62017
      #socket = /var/run/uwsgi/apphello.sock
      # Ensure socket has perms/group wide enough for your web server
      chown-socket = uwsgi:nginx
      chmod-socket = 664
      # Clear environment on exit by setting vacuum
      vacuum = true
  - backup: minion


uwsgi_app_py2:
  file.managed:
  - name: {{ approot }}/apphello.wsgi
  - user: root            # {{ giuser }}
  - group: {{ gigroup }}
  - mode: 644
  - contents: |
      def application(environ, start_response):
        start_response('200 OK', [('Content-Type', 'text/plain')])
        return b'Hello World!'
      # Generated from saltstack item uwsgi_app_py2
  - backup: minion


nginx_app_conf:
  file.managed:
  - name: /etc/nginx/conf.d/apphello.conf
  - contents: |
      # Nginx upstream block - component nginx needs to connect to
      upstream upstream1 {
      	server localhost:62017;
      }
      
      # Nginx server block
      server {
      	listen		8080;
      	server_name 	apphello.example.com;
      	charset		utf-8;
      	client_max_body_size 75M;    # max upload size
      
      	location /static {
      		alias /usr/share/nginx/html;
      	}
      
        # Dynamic results (not already captured above) go here
      location /dynamic {
      		uwsgi_pass upstream1;
      		include uwsgi_params;
      		
      		uwsgi_param Host $host;
      		uwsgi_param X-Real-IP $remote_addr;
      		uwsgi_param X-Forwarded-For $proxy_add_x_forwarded_for;
      		uwsgi_param X-Forwarded-Proto $http_x_forwarded_proto;
      	}
      }
  - backup: minion

