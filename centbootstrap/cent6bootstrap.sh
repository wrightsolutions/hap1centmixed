#!/bin/sh
# Centos targetting bootstrapper to ensure stable (repo provided) salt-call available
# This version tested on Centos 6 and amzn1
yum install epel-release
/bin/sed -i '0,/gpgcheck=/{s$enabled\=0$enabled\=1$}' /etc/yum.repos.d/epel.repo
yum install salt-ssh salt salt-minion zile
# We only need minion for the binary salt-call so disable it but keep the binaries
chkconfig --levels 0123456 salt-minion off
# Make a directory below /srv/ for where salt-call expects to find .sls files by default
/bin/mkdir -p /srv/salt
printf "Final action is to report on whereabouts of salt-call binary:\n"
whereis salt-call
exit 0
# amzn2: amazon-linux-extras install epel
# /usr/bin/salt-call --local state.sls nginx112uwsgi
